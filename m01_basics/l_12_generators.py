from util.create_utils import create_devices, create_device, create_device_gen
import time

if __name__ == '__main__':

    devices = create_devices(254, 254)

    devices_dict = dict()
    for device in devices:
        devices_dict[device["ip"]] = device

    devices_gen = create_device_gen(254, 254)

    while True:

        ip_to_find = input("\nEnter IP address to find: ")
        if not ip_to_find:
            break

        start = time.time()
        for device in devices_gen:
            if device["ip"] == ip_to_find:
                print(f"---> Found it (generator): {device}")
                generator_search_time = (time.time() - start) * 1000
                print(f"--- ---> in: {round(generator_search_time, 2)} mesc")
                print(f"--- ---> id of device:", id(device))
                break
        else:
            print(f"----! IP address not found, try again")

        start = time.time()
        if ip_to_find in devices_dict:
            print(f"---> Found it (dict): {device}")
            dict_search_time = (time.time() - start) * 1000
            print(f"--- ---> in: {round(dict_search_time, 2)} mesc")
            print(f"--- ---> id of device:", id(devices_dict[ip_to_find]))

        print(f"conclusion: dictionary search was {int(generator_search_time/dict_search_time)}"
              f" times faster than generator")

    '# SIMPLE GENERATORS COMPREHENSIONS'
    device_str = "  r3-L-n7, cisco, caralyst 2960, ios, extra stupid stuff"
    device = [item.strip() for item in (device_str.split(","))]
    print("device using generator comprehension:\n\t\t", device)

    print("\n\n----- DEVICE CREATION USING GENERATOR COMPREHENSION -----")
    device_gen = (create_device(i, 1) for i in range(1, 25))
    for device in device_gen:
        print(device)
