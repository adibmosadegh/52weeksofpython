from util.create_utils import create_devices
from pprint import pprint
from operator import itemgetter
from tabulate import tabulate
from datetime import datetime
from time import sleep
import nmap

devices = create_devices(5, 5)

print("\n\nUsing Print")
print(devices)

print("\n\nUsing Pprint")
pprint(devices)

print("\n\nUsing loop")
for device in devices:
    sleep(0.1)
    device["last_heard"] = str(datetime.now())
    print(device)

print("\n\nUsing Tabulate")
print(
     tabulate(sorted(devices, key=itemgetter("vendor", "os", "version")), headers="keys")
 )

print("\n\nUsing for loop and fstring")
print("    Name    Vendor      os     IP Address      Last Heard")
print("    ----    ------      ---    -----------     -----------")
for device in devices:
    print(
        f'{device["name"]:>7}    {device["vendor"]:>8}  : {device["os"]:<5}   {device["ip"]:<15}  '
        f'{device["last_heard"][:-4]}'
    )

print("\n\nSame thing, but sorted")
for device in sorted(devices, key=itemgetter("last_heard"), reverse=True):
    print(
        f'{device["name"]:>7}    {device["vendor"]:>8}  : {device["os"]:<5}   {device["ip"]:<15}  '
        f'{device["last_heard"][:-4]}'
    )

print("\n\nMultiple Print Statements, Same Line")
print("Testing Devices: ")
for device in devices:
    print(f"--- testing {device['name']} ...", end="")
    print("done.")
print("Testing Completed")

nm = nmap.PortScanner()
while True:
    ip = input("\nInput IP Address to scan: ")
    if not ip:
        break

    print(f"\n--- biginning scan of {ip}")
    output = nm.scan(ip, '22-1024')
    print(f"--- --- command: {nm.command_line()}")

    print("------ namp scan output ------")
    print(output)
