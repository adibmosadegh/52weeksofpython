from util.create_utils import create_devices
from pprint import pprint
from random import uniform
from random import randint
from datetime import datetime
from copy import copy
from copy import deepcopy

devices = create_devices(5, 5)
print("\n    Name     Vendor  :  OS     IP Address     version")
print("     -----     ------    ---    ----------    --------")
for device in devices:
    print(
        f"{device['name']:>9}  {device['vendor']:>10} : {device['os']:>5}   {device['ip']:>8}  {device['version']:>10}"
    )

print("\n    Name      Vendor  :  OS     IP Address     version")
print("     -----     ------    ---    ----------    --------")
for device in devices:
    if device['vendor'].lower() == "cisco":
        print(
            f"{device['name']:>9}  {device['vendor']:>10} : {device['os']:>5}   {device['ip']:>8}  "
            f"{device['version']:>10}"
        )

print("\n ----- starting comparison of device names -----")
devices_with_same_name = list()
for index, device_a in enumerate(devices):
    for device_b in devices[index + 1:]:
        if device_a["name"] == device_b["name"]:
            print(
                f"Found match! device {device_a['name']} with ip {device_a['ip']} and device {device_b['name']} "
                f"with ip {device_b['ip']}")
            devices_with_same_name.append(device_b)
if len(devices_with_same_name) == 0:
    print("No device with the same name was found")
print(" ----- comparison of devices names completed! -----")

print("\n ---- create table of arbitrary 'standard' version for each vendor:os ----")
standard_version = dict()
for device in devices:
    vendor_os = device["vendor"] + ":" + device["os"]
    if vendor_os not in standard_version:
        standard_version[vendor_os] = device["version"]
pprint(standard_version)

print("\n ---- Create list of non-compliant device OS versios for each vendor:os ---------")
non_compliant_devices = dict()
for vendor_os, _ in standard_version.items():
    non_compliant_devices[vendor_os] = []

for device in devices:
    vendor_os = device["vendor"] + ":" + device["os"]
    if device["version"] != standard_version[vendor_os]:
        non_compliant_devices[vendor_os].append(device["ip"] + " version: " + device["version"])
pprint(non_compliant_devices)

print("\n\n ----- Assignment, copy, and deep copy ----------")
devices2 = devices
devices[0]["name"] = "this is a dumb device name"
if devices2 == devices:
    print("\n    Assignment and modification: device2 STILL equals devices")
    print("    ----> Moral: Assignment is Not the same as copy!")
else:
    print("    Huh?")


devices2 = copy(devices)
devices[0]["name"] = "this also is a dumb device name"
if devices2 == devices:
    print("\n    Shallow copy and modification: devices2 Still equals devices!")
    print("    ----> Moral: copy() only does a Shallow (1st level) copy!")
    print("    ----> Result: Uh-oh - I just screwed up the original version!!")
else:
    print("    Huh?")

devices2 = deepcopy(devices)
devices[0]["name"] = "This is Another dumb device name"
if devices2 == devices:
    print("    Huh?")
else:
    print("\n    Deep copy and modification: devices2 no linger equals devices")

new_set_of_devices = create_devices(5, 5)
if new_set_of_devices == devices:
    print("    Huh?")
else:
    print("\n    Comparison of complex, deep data is easy in python")

print("\n\n ----- comparisons for implementing SLAs -----------------")
SLA_AVAILABILITY = 96
SLA_RESPONSE_TIME = 1.0

devices = create_devices(2, 25)
for device in devices:
    device["availability"] = randint(94, 100)
    device["response_time"] = uniform(0.5, 1.1)

    if device["availability"] < SLA_AVAILABILITY:
        print(f"{datetime.now()}: {device['name']:6} - Availability {device['availability']} < 96")
    if device["response_time"] > SLA_RESPONSE_TIME:
        print(f"{datetime.now()}: {device['name']:6} - Response Time {round(device['response_time'], 3)} > 1.0")

print("\n\n----- Comparing classes --------------")


class DeviceWithEq:

    def __init__(self, name, ip):
        self.name = name
        self.ip_address = ip

    def __eq__(self, other):
        if not isinstance(other, DeviceWithEq):
            return False
        return self.name == other.name and self.ip_address == other.ip_address


d1 = DeviceWithEq("Device 1", "10.10.10.1")
d1_equal = DeviceWithEq("Device 1", "10.10.10.1")
d1_diffrent = DeviceWithEq("Device 2", "10.10.10.2")

print("\n    Comparison with an __eq__ method to handle evaluation")
if d1 == d1_equal:
    print(f"    ----> Using __eq__ : success: {d1} equals {d1_equal}")
else:
    print(f"    !!! using __eq__ : uh-oh, classes not equal and they should be")
if d1 == d1_diffrent:
    print(f"    !!! using __eq__ : uh-oh, classes not equal and they should not be")
else:
    print(f"    ----> Using __eq__ : success: {d1} not equal to {d1_equal}")


class DeviceWithNoEq:

    def __init__(self, name, ip):
        self.name = name
        self.ip_address = ip


d1 = DeviceWithNoEq("Device 1", "10.10.10.1")
d1_equal = DeviceWithNoEq("Device 1", "10.10.10.1")
d1_same = d1

print("\n    Comparison without an __eq__ method to handle evaluation")
if d1 == d1_equal:
    print(f"    !!! with no __eq__ : Oops, didn't expect this")
else:
    print(f"    ----> with no __eq__ : success: not equal, as expected")
if d1 == d1_same:
    print(f"    ----> with no __eq__ : success: {d1} points to same object")
else:
    print(f"    !!! with no __eq__ : Oops, didn't expect this")
