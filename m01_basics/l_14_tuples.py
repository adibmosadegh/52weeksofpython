from util.create_utils import create_devices_tuple
from util.create_utils import create_devices
from pprint import pprint
from collections import namedtuple

if __name__ == '__main__':

    devices = create_devices_tuple(4, 1)

    print("\n----- LIST OF DEVICES ----------")
    pprint(devices)

    print("\n---- DEVICE AS TUPLE ---------")
    device = ("sbx-n9kv-ao", "cisco", "Nexus9000 C9300v Chassis", "nxos", "10.0.1.1")

    print("  name:", device[0])
    print("vendor:", device[1])
    print(" model:", device[2])
    print("    os:", device[3])
    print("    ip:", device[4])

    print("\n------ DEVICE AS NAME TUPLE -------")
    Device = namedtuple("Device", ["name", "vendor", "model", "os", "ip"])
    device = Device("sbx-n9kv-ao", "cisco", "Nexus9000 C9300v Chassis", "nxos", "10.0.1.1")

    print("  name:", device.name)
    print("vendor:", device.vendor)
    print(" model:", device.model)
    print("    os:", device.os)
    print("    ip:", device.ip)

    print("\n---- PRINT OF DEVICE NAME TUPLE -----")
    pprint(devices)

    print("\n---- CONVERT DEVICE TO NAMES TUPLES -----")
    devices = create_devices(10, 2, True)
    devices_as_namedtuple = list()
    for device in devices:
        Device = namedtuple("Device", device.keys())
        devices_as_namedtuple.append(Device(**device))

    print("\n----- CONVERT DEVICES TO NAMED TUPLE ------------")
    pprint(devices)

    print("\n------ PRINT VERSION OF DEVICES AS NAMED TUPLE ----------")
    print("    NAME     VENDOR  :  OS      IP ADDRESS")
    print("   -----     ------    ----     -----------")
    for device in devices_as_namedtuple:
        print(f'{device.name:>7}  {device.vendor:>10}  :  {device.os:<6}   {device.ip:<15}')
