from util.create_utils import create_devices
from tabulate import tabulate
import time
if __name__ == "__main__":

    devices = create_devices(254, 254)

    devices_dict = dict()
    for device in devices:
        devices_dict[device["ip"]] = device

    print("calculating tabular output of devices ...")
    print("\n", tabulate(devices, headers="keys"))

    while True:

        ip_to_find = input("\nEnter IP address to find: ")
        if not ip_to_find:
            break

        start = time.time()
        for device in devices:
            if device["ip"] == ip_to_find:
                print(f"----> Found it (list): {device}")
                list_search_time = (time.time() - start) * 1000
                print(f"---- ----> in: {round(list_search_time, 2)} msec")
                print(f"---- ----> id of device:", id(device))
                break
        else:
            print(f"----! IP address not found, try again.")
            continue

        start = time.time()
        if ip_to_find in devices_dict:
            print(f"----> Found it (dict): {devices_dict[ip_to_find]}")
            dict_search_time = (time.time() - start) * 1000
            print(f"---- ----> in: {round(list_search_time, 2)} msec")
            print(f"---- ----> id of device:", id(devices_dict[ip_to_find]))

        print(f"conclusion: dictionary search was {int(list_search_time/dict_search_time)} "
              f"time faster than list search.")
