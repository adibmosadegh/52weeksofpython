device1_str = "   r3-L-n7, cisco, catalyst 2960, ios   "

'#using split to adding '' to any value'
device1 = device1_str.split(",")
print("Device1 using split :")
print(f"     {device1}")

'#using Strip and split'
device1 = device1_str.strip().split(",")
print("Device1 using strip and split togheter")
print(f"     {device1}")

'#remove blanks without using strip'
device1 = device1_str.replace(" ", "").split(",")
print("Device1 using replace to remove blanks:\n    ", device1)

'#Remove blanks and change comma to colon'
device1_str_colon = device1_str.replace(" ", "").replace(",", ":")
print("Device1 using from replace to remove blanks and , :")
print(f"     {device1_str_colon}")

'#Loop with strip and split'
device1 = list()
for item in device1_str.split(","):
    device1.append(item.strip())
print("Device1 use loop and strip and split : ")
print(f"     {device1}")

'#list comprehension'
device1 = [item.strip() for item in device1_str.split(",")]
print("Device1 using list comprehension: ")
print(f"     {device1}")

'#Ignoring case'
print("\n\nIgnoring case")
model = "CSR1000V"
if model == "csr1000v":
    print(f"matched: {model}")
else:
    print(f"Didn't match: {model}")

if model.lower() == "csr1000v":
    print(f"matched using lower(): {model}")
else:
    print(f"Didn't match: {model}")


'#Finding Substring'
print("\n\nFinding Substring")
version = "Virtual XE Software (X86_64_LINUX_IOSD_UNIVERSALK9-M), Version 16.11.1a"
expected_version = "Version 16.11.1a"
index = version.find(expected_version)
if index >= 0:
    print(f"Found version: {expected_version} at location {index} ")
else:
    print(f"not foud: {expected_version}")

'#seprating string components'
print("\n\n Seprating Version String Components")
version_info = version.split(",")
for part_no, version_info_part in enumerate(version_info):
    print(f"Version part {part_no+1}: {version_info_part.strip()}")


show_interface_states = """
GigabitEthernet1
        Switching path          Pkt In      Chars In      Pkts Out     Chars Out   
        pricessor                25367       1529598       8242         494554
        Route cache                0            0           0              0   
        Distrabiuted cache       24234         234          2334        234234
        Total                    1231455       124124      1241234      1435345
        
GigabitEthernet2
        Switching path          Pkt In      Chars In      Pkts Out     Chars Out   
        pricessor                25367       1529598         0         494537
        Route cache                0            0            0              0   
        Distrabiuted cache       234554         0          2334        23434564
        Total                    12315        1241         1241234      12345
"""
interface_counter = dict()
show_interface_states_lines = show_interface_states.splitlines()
for index, states_line in enumerate(show_interface_states_lines):
    if states_line.find("GigabitEthernet", 0) == 0:

        totals_line = show_interface_states_lines[index + 5]
        interface_counter[states_line] = totals_line.split()[1:]

print("\n\n________Interface Counter_________")
print(interface_counter)
