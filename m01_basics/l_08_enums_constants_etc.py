from random import choice
import string
from tabulate import tabulate
from enum import Enum

#Using Constant variables

# CISCO = "cisco"
# JUNIPER = "juniper"
# ARISTA = "arista"
#
# for index in range(20):
#
#     device = dict()
#
#     device["name"] = (
#             choice(["r2", "r3", "r4", "r6", "r10"])
#             + choice(["L", "U"])
#             + choice(string.ascii_letters)
#     )
#
#     device["vendor"] = choice([CISCO, JUNIPER, ARISTA])
#     if device["vendor"] == CISCO:
#         device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
#         device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
#     elif device["vendor"] == JUNIPER:
#         device["os"] = "eos"
#         device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])
#     elif device["vendor"] == ARISTA:
#         device["os"] = "junus"
#         device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])
#
#     pprint(device)

#Using Class

# class Vendor:
#     CISCO = "cisco"
#     ARISTA = "arista"
#     JUNIPER = "juniper"
#
# def create_device(num_devices):
#     created_devices = list()
#     for index in range(0,num_devices):
#
#
#         device = dict()
#
#         device["name"] = (
#                 choice(["r2", "r3", "r4", "r6", "r10"])
#                 + choice(["L", "U"])
#                 + choice(string.ascii_letters)
#         )
#
#         device["vendor"] = choice([Vendor.CISCO, Vendor.ARISTA, Vendor.JUNIPER])
#         if device["vendor"] == Vendor.CISCO:
#             device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
#             device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
#         elif device["vendor"] == Vendor.JUNIPER:
#             device["os"] = "eos"
#             device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])
#         elif device["vendor"] == Vendor.ARISTA:
#             device["os"] = "junus"
#             device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])
#
#         created_devices.append(device)
#
#     return created_devices
#
# devices = create_device(10)
# print(
#     tabulate(devices, headers="keys")
# )

#Using enums class
# class Vendor(Enum):
#     CISCO = 1
#     JUNIPER = 2
#     ARISTA = 3
#
# def create_device(num_devices):
#     created_devices = list()
#     for index in range(0,num_devices):
#
#
#         device = dict()
#
#         device["name"] = (
#                 choice(["r2", "r3", "r4", "r6", "r10"])
#                 + choice(["L", "U"])
#                 + choice(string.ascii_letters)
#         )
#
#         device["vendor"] = choice([Vendor.CISCO.name, Vendor.ARISTA.name, Vendor.JUNIPER.name])
#         if device["vendor"] == Vendor.CISCO.name:
#             device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
#             device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
#         elif device["vendor"] == Vendor.JUNIPER.name:
#             device["os"] = "eos"
#             device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])
#         elif device["vendor"] == Vendor.ARISTA.name:
#             device["os"] = "junus"
#             device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])
#
#         created_devices.append(device)
#
#     return created_devices
#
# devices = create_device(10)
# print(
#     tabulate(devices, headers="keys")
# )


#Using Enum class with value
class Vendor(Enum):
    CISCO = "cisco"
    JUNIPER = "juniper"
    ARISTA = "arista"

def create_device(num_devices):
    created_devices = list()
    for index in range(0,num_devices):


        device = dict()

        device["name"] = (
                choice(["r2", "r3", "r4", "r6", "r10"])
                + choice(["L", "U"])
                + choice(string.ascii_letters)
        )

        device["vendor"] = choice([Vendor.CISCO.value, Vendor.ARISTA.value, Vendor.JUNIPER.value])
        if device["vendor"] == Vendor.CISCO.value:
            device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
            device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
        elif device["vendor"] == Vendor.JUNIPER.value:
            device["os"] = "eos"
            device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])
        elif device["vendor"] == Vendor.ARISTA.value:
            device["os"] = "junus"
            device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])

        created_devices.append(device)

    return created_devices

devices = create_device(10)
print(
    tabulate(devices, headers="keys")
)