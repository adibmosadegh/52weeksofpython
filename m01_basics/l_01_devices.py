from pprint import pprint

devices = {
    "name": "sbx_n9kv_ao",
    "vendor": "cisco",
    "model": "Nexus9000 C9300v Chassis",
    "os": "nxos",
    "version": "9.3(3)",
    "ip": "10.1.1.1",
    1: "any data goes here"
}

'#simple print'
print("\n________Simple Print________")
print("Devices:", devices)
print("Devise name:", devices["name"])

'#prety print'
print("\n________Prety Print________")
pprint(devices)

'#For Loop nicely formatted print'
print("\n________For Loop, Using f-string________")
for key, value in devices.items():
    print(f"{str(key):>15s} : {value}")
