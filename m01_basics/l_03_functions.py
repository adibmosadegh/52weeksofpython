from random import choice
import string
from tabulate import tabulate


def create_devices(num_devices=1, num_subnets=1):

    created_devices = list()

    if num_devices > 254 or num_subnets > 254:
        print("Error: too many devices and/or subnets requested")
        return created_devices

    for subnet_index in range(1, num_subnets+1):

        for device_index in range(1, num_devices+1):

            device = dict()

            device["name"] = (
                choice(["r2", "r3", "r4", "r6", "r10"])
                + choice(["L", "U"])
                + choice(string.ascii_letters)
            )

            device["vendor"] = choice(["cisco", "juniper", "arista"])
            if device["vendor"] == "cisco":
                device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
                device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
            elif device["vendor"] == "juniper":
                device["os"] = "junus"
                device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])
            elif device["vendor"] == "arista":
                device["os"] = "eos"
                device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])

            device["ip"] = "10.0." + str(subnet_index) + "." + str(device_index)

            created_devices.append(device)

    return created_devices, "By Adib Moshadegh"


if __name__ == "__main__":
    devices, s = create_devices(num_devices=20, num_subnets=14)
    print("\n", tabulate(devices, headers="keys"))
    print(s)
