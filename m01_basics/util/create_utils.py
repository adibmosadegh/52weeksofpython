from random import choice
import string
from random import randint

def get_random_ip(subnet_index):
    ip = "10.0." + str(subnet_index) + "." + str(randint(1, 244))

    return ip

def create_device(device_index, subnet_index, random_ip):
    device = dict()

    device["name"] = (
            choice(["r2", "r3", "r4", "r6", "r10"])
            + choice(["L", "U"])
            + choice(string.ascii_letters)
    )

    device["vendor"] = choice(["arista", "juniper", "cisco"])
    if device["vendor"] == "cisco":
        device["os"] = choice(["ios", "iosxe", "iosxr", "nexus"])
        device["version"] = choice(["12.1.04", "14.07", "8.12", "20.45"])
    elif device["vendor"] == "arista":
        device["os"] = "eos"
        device["version"] = choice(["4.24", "4.23", "4.22", "4.21"])
    elif device["vendor"] == "juniper":
        device["os"] = "junus"
        device["version"] = choice(["12.3", "15.1", "18.4", "15.1"])

    if random_ip:
        device["ip"] = get_random_ip(subnet_index)
    else:
        device["ip"] = "10.0." + str(subnet_index) + "." + str(device_index)

    return device


def create_devices(num_devices, num_subnets, random_ip=False):

    created_devices = list()

    if num_devices > 254 or num_subnets > 254:
        print("Error: too many devices or sub requested")
        return created_devices

    for subnet_index in range(1, num_subnets+1):

        for device_index in range(1, num_devices+1):

            device = create_device(device_index, subnet_index, random_ip)
            created_devices.append(device)

            created_devices.append(device)

    print("completed device creation")
    return created_devices


def create_devices_tuple(num_devices=1, num_subnets=1):

    return tuple(create_devices(num_devices=num_devices, num_subnets=num_subnets))


def create_networks(num_devices=1, num_subnets=1):
    devices = create_devices(num_devices, num_subnets)

    network = dict()
    network["subnets"] = dict()

    for device in devices:
        subnet_address_bytes = device["ip"].split(".")
        subnet_address_bytes[3] = "0"
        subnet_address = ".".join(subnet_address_bytes)

        if subnet_address not in network["subnets"]:
            network["subnets"][subnet_address] = dict()
            network["subnets"][subnet_address]["devices"] = list()

        network["subnets"][subnet_address]["devices"].append(device)

        interfaces = list()
        for index in range(0, choice([2, 4, 8])):
            interface = {
                "name": "g/0/0" + str(index),
                "speed": choice(["10", "100", "1000"])
            }
            interfaces.append(interface)

        device["interface"] = interfaces

    return network


def create_device_gen(num_devices=1, num_subnets=1):
    if num_devices > 254 or num_subnets > 254:
        print("Error: too many devices and/or subnets requested")
        return None

    print("Beginning device creation")
    for subnet_index in range(1, num_subnets + 1):

        for device_index in range(1, num_devices + 1):
            device = create_device(device_index, subnet_index)
            yield device
