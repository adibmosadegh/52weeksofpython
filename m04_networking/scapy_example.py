import scapy.all as scapy
from scapy.layers.l2 import Ether, ARP
from scapy.layers.inet import TCP, ICMP, IP

# print("\n--- Packet summeries ------")
# capture = scapy.sniff(iface="wlp2s0", count=10)
# print(capture.nsummary())
#
# print("\n--- DNS packet summeries (collect 10 DNS packets) --------------------------")
# capture = scapy.sniff(iface="wlp2s0", filter="udp port 53", count=10)
# print(capture.nsummary())
#
# print("\n\n--- DNS packet, complete (collect 10 DNS packets) ------------------------")
# capture = scapy.sniff(iface="wlp2s0", filter="udp port 53", count=10)
# for packet in capture:
#     print(packet.show())
#
# print("\n--- Capture and print packet as sniffed ------------------------------------")
#
#
# def print_packet(pkt):
#     print("    ", pkt.summary())
#
#
# scapy.sniff(iface="wlp2s0", prn=print_packet, filter="udp port 53", count=10)
#
#
# print("\n--- capture and print packets as sniffed (sniffed lambda) -------------------")
# scapy.sniff(iface="wlp2s0", prn=lambda pkt: print(f" lambda {pkt.summary()}"), filter="udp port 53", count=10)
#

# print("\n\n--- Discovery hosts on network using manual ARP ping ---------------------")
# ans, unans = scapy.srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst="192.168.1.0/24"), timeout=2)
# ans.summary()

# for res in ans.res:
#     print(f"----> IP address discovered: {res[0].payload.pdst}")

print("\n\n--- See what ports are open on a device ----------------------------------")
while True:

    ip = input("IP address on which to scan ports: ")
    if not ip:
        print("\n--- ending port scanning")
        break

    answers, unans = scapy.sr(IP(dst=ip)/TCP(flags="S", sport=666, dport=(1, 1024)), timeout=10)
    for answered in answers:
        print(f"----> Open port: {answered[0].summary()}")

    print(f"\n\t Open ports: {len(answered)}")
    print(f"\n\t Open ports: {len(unans)}")
