from l_00_inventory import csv_inventory
import csv
from pprint import pprint
from tabulate import tabulate
import filecmp

with open("l_00_inventory.csv", "w") as csv_out:
    csv_writer = csv.writer(csv_out)
    csv_writer.writerows(csv_inventory)

with open("l_00_inventory.csv", "r") as csv_in:
    csv_reader = csv.reader(csv_in)
    saved_csv_inventory = list()
    for device in csv_reader:
        saved_csv_inventory.append(device)

print("l_00_inventory.csv file:\n", saved_csv_inventory)

print("\ncsv pretty version:")
pprint(saved_csv_inventory)

print("\n--- compare saved inventory with original ---")
if saved_csv_inventory == csv_inventory:
    print("-- worked: saved inventory equals original")
else:
    print("-- failed: saved inventory diffrent from original")


devices = list()
for device_index in range(1, len(csv_inventory)):
    device_dict = dict()
    for index, headers in enumerate(csv_inventory[0]):
        device_dict[headers] = csv_inventory[device_index][index]
    devices.append(device_dict)

print("\n----- Devices as list of dicts ------------------------")
pprint(devices)

with open("devices_for_csv_example - Sheet1.csv", "r") as csv_in:
    csv_reader = csv.reader(csv_in)
    from_spreadsheet_csv_inventory = list()
    for device in csv_reader:
        from_spreadsheet_csv_inventory.append(device)

devices = list()
for device_index in range(1, len(from_spreadsheet_csv_inventory)):
    device = dict()
    for index, headers in enumerate(from_spreadsheet_csv_inventory[0]):
        device[headers] = from_spreadsheet_csv_inventory[device_index][index]
    devices.append(device)

print("\n----- Devices from spreadsheet ------------------------")
pprint(devices)

print("\n---- tabulate output of devices from speadsheet ------------")
print("\n", tabulate(devices, headers="keys"))

headers = devices[0].keys()
with open("l_00_inventory_back_to_csv.csv", "w") as csv_out:
    csv_writer = csv.DictWriter(csv_out, headers)
    csv_writer.writeheader()
    csv_writer.writerows(devices)

